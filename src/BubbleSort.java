/***
 * 冒泡排序：每当相邻的两数比较后发现他们的排序与要求相反时，互换
 * 算法复杂度：O(n^2)
 */
public class BubbleSort {

	public void sort(int[] a) {
		// TODO Auto-generated method stub
		for(int i = 0; i < a.length; i++){
			for(int j = i+1; j < a.length; j++){
				if(a[i] > a[j]){
					int temp = a[j];
					a[j] = a[i];
					a[i] = temp;
				}
			}
		}
	}

}
