/***
 * 交换排序--快速排序(Quick Sort)
 * 不稳定排序
 * 最坏情况：O(n^2) 平均情况：O(nlogn)
 */
public class QuickSort {

	public void sort(int[] a, int low, int high) {
		// TODO Auto-generated method stub
		if(low < high){
			sort(a, low, partition(a, low, high)-1);
			sort(a, partition(a, low, high)+1, high);
		}
	}

	private int partition(int[] a, int low, int high) {
		// TODO Auto-generated method stub
		int pivot = a[low];
		while(low < high){
			while(low < high && a[high] >= pivot){
				high--;
			}
			a[low] = a[high];
			while(low < high && a[low] <= pivot){
				low++;
			}
			a[high] = a[low];
		}
		a[low] = pivot;
		return low;
	}
}
