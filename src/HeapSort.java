
public class HeapSort {
	public void sort(int[] array){
		buildHeap(array);
		for(int i = array.length-1; i > 0; i--){
			swap(array, 0, i);
			heapify(array, 0, i-1);
		}
	}

	private void heapify(int[] array, int index, int maxIndex) {
		// TODO Auto-generated method stub
		int leftChild = 2*index+1;
		int rightChild = 2*index+2;
		int largestIndex = 0;
		if(leftChild <= maxIndex && array[leftChild] > array[index]){
			largestIndex = leftChild;
		}
		else{
			largestIndex = index;
		}
		if(rightChild <= maxIndex && array[rightChild] > array[largestIndex]){
			largestIndex = rightChild;
		}
		if(largestIndex != index){
			swap(array, index, largestIndex);
			heapify(array, largestIndex, maxIndex);
		}
	}

	private void swap(int[] array, int i, int j) {
		// TODO Auto-generated method stub
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	private void buildHeap(int[] array) {
		// TODO Auto-generated method stub
		for(int i = array.length/2-1; i >= 0; i--){
			heapify(array, i, array.length-1);
		}
	}
}
