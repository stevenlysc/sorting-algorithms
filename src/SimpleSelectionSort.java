/***
 * 选择排序--简单选择排序
 * 选择排序与冒泡排序区别：冒泡排序每次比较后，如果发现顺序不对立即交换，而选择排序在找到最小的元素后才进行交换
 * 算法复杂度：O(n^2)
 */
public class SimpleSelectionSort {

	public void sort(int[] a) {
		// TODO Auto-generated method stub
		for(int i = 0; i < a.length; i++){
			int temp = Integer.MAX_VALUE;
			int index = 0;
			for(int j = i; j < a.length; j++){
				if(a[j] < temp){
					temp = a[j];
					index = j;
				}
			}
			a[index] = a[i];
			a[i] = temp;
		}
	}
}
