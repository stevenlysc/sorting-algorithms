/***
 * 插入排序--直接插入排序
 * 要点：设立哨兵，作为临时存储和判断数组边界之用
 * 算法复杂度：O(n^2)
 */
public class StraightInsertionSort {

	public void sort(int[] a) {
		// TODO Auto-generated method stub
		for(int i = 1; i < a.length; i++){
			int temp = a[i];
			for(int j = i-1; j >= 0; j--){
				if(temp < a[j]){
					a[j+1] = a[j];
					a[j] = temp;
				}
			}
			/*if(a[i] < a[i-1]){
				int j = i-1;
				int x = a[i];
				try{
					while(x < a[j]){
						a[j+1] = a[j];
						j--;
					}
					a[j+1] = x;
				}
				catch(IndexOutOfBoundsException e){
					a[0] = x;
				}
			}*/
		}
	}
}
