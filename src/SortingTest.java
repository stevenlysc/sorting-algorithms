import java.util.Random;

public class SortingTest {
	static int[] array = new int[10];	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		StraightInsertionSort insertsort = new StraightInsertionSort();
		arrayGenerator(array);
		insertsort.sort(array);
		toString(array);
		
		BubbleSort bubblesort = new BubbleSort();
		arrayGenerator(array);
		bubblesort.sort(array);
		toString(array);
		
		SimpleSelectionSort selectionsort = new SimpleSelectionSort();
		arrayGenerator(array);
		selectionsort.sort(array);
		toString(array);
		
		QuickSort quicksort = new QuickSort();
		arrayGenerator(array);
		quicksort.sort(array, 0, array.length-1);
		toString(array);
		
		MergeSort mergesort = new MergeSort();
		arrayGenerator(array);
		mergesort.sort(array);
		toString(array);
		
		HeapSort heapsort = new HeapSort();
		arrayGenerator(array);
		heapsort.sort(array);
		toString(array);
	}

	private static void arrayGenerator(int[] array) {
		// TODO Auto-generated method stub
		Random random = new Random(23);
		for(int i = 0; i < array.length; i++){
			array[i] = random.nextInt(50);
		}
	}

	private static void toString(int[] a) {
		// TODO Auto-generated method stub
		for(int i = 0; i < a.length; i++){
			System.out.print(a[i] + " ");
		}
		System.out.println();
	}

}
